<?php
session_start();
$gender_list = ["Nam", "Nữ"];
$faculty_list = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

$userName = $_SESSION['userName'];
$gender = $gender_list[$_SESSION['gender'] - 1];
$faculty = $faculty_list[$_SESSION['faculty']];
$birthday = $_SESSION['birthday'];
$address = $_SESSION['address'];
if (isset($_SESSION['img'])) {
    $img = $_SESSION['img'];
} else {
    $img = '';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/signup.css">
    
</head>

<body>
    <div class="wrapper">
        <form method="post" enctype="multipart/form-data">
            <div class="input-box faculty-box">
                <label for="faculty" class="label-input">
                    Khoa
                </label>
               <select name="faculty" id="faculty" class="select-field">                
                    <?php foreach ($faculty_list as $key => $value) { ?>
                        <option value=<?=$key?> <?=(isset($_POST['faculty']) && $_POST['faculty'] === $key) ? 'selected' : ''; ?>><?php echo $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="input-box name-box">
                <label for="" class="label-input">
                    Từ khoá
                </label>
                <input type="text" class="text-field" name="searchkey" id="searchkey" value='<?php if(isset($_POST['searchkey']) && $_POST['searchkey'] != NULL){ echo $_POST['searchkey']; } ?>'>
            </div>
            <div class="searchbutton" style=" position: relative;">
                <div class="button-box" style="position: absolute;right: 55%;">
                    <input class="button-submit" value="Xoá" type="button" onclick="ClearFields()">
                </div>
                <div class="button-box" style="position: absolute; left: 50%;">
                    <button class="button-submit" type="submit">Tìm kiếm</button>
                </div>
            </div>
            <br>
            
            <br>
        </form>
        <div class="input-box name-box" style=" padding: 10px;">
            <p style="float: left;">Số sinh viên tìm thấy: XXX</p>
            <form action="signup.php" style="float: right;">
                <div>
                    <button class="button-submit" type="submit">Thêm</button>
                </div>
            </form>
        </div>
        <div class="list_student">
            <table style="width:100%">
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <div class="action-button">
                            <form action="">
                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Xoá</button>

                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Sửa</button>
                            </form>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <div class="action-button">
                            <form action="">
                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Xoá</button>

                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Sửa</button>
                            </form>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <div class="action-button">
                            <form action="">
                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Xoá</button>

                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Sửa</button>
                            </form>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <div class="action-button">
                            <form action="">
                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Xoá</button>

                                <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.5;color: white;">Sửa</button>
                            </form>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
<script>
    function ClearFields() {
        document.getElementById("searchkey").value = "";
        document.getElementById("faculty").value = "";
    }
</script>